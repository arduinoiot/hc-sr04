// defines pins numbers
const int trigPin = 13;
const int echoPin = 12;
#define buzzerPin  8
#define ledPin  2
#define f1  1000
#define f2   2000
int btnState = 0;

// defines variables
long duration;
int distance;

void setup() {
  pinMode(trigPin, OUTPUT); // Sets the trigPin as an Output
  pinMode(echoPin, INPUT); // Sets the echoPin as an Input
  pinMode(buzzerPin, OUTPUT);
  pinMode(ledPin, OUTPUT);
  Serial.begin(9600); // Starts the serial communication
}

void loop() {
  // Clears the trigPin
  digitalWrite(trigPin, LOW);
  delayMicroseconds(2);
  digitalWrite(ledPin, LOW);
  // Sets the trigPin on HIGH state for 10 micro seconds
  digitalWrite(trigPin, HIGH);
  delayMicroseconds(10);
  digitalWrite(trigPin, LOW);

  // Reads the echoPin, returns the sound wave travel time in microseconds
  duration = pulseIn(echoPin, HIGH);

  // Calculating the distance
  distance = duration * 0.034 / 2;
  //check the range
  if (distance >= 300) {
    Serial.println("Out of range");
  } else {
    // buz the buzzer
    if (distance <= 100) {
      digitalWrite(ledPin, HIGH);
      tone(buzzerPin, f1);
      delay(500);
      noTone(buzzerPin);
      digitalWrite(ledPin, LOW);
    } else {
      noTone(buzzerPin);
      digitalWrite(ledPin, LOW);
      delay(500);
    }
    // Prints the distance on the Serial Monitor
    Serial.print("Distance: ");
    Serial.print(distance);
    Serial.println(" cm ");
  }

  delay(1000);
}
